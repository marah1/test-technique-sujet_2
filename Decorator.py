import logging
logger = logging.getLogger('decorator-log')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter

formatter = logging.Formatter('[%(asctime)s][file:%(filename)s][%(levelname)s]  %(message)s')
# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)
import functools
class LogDecorator(object):
    def __init__(self):
        self.logger = logging.getLogger('decorator-log')
        self._locals = {}

    def __call__(self, fn):
        @functools.wraps(fn)
        def tracer(frame, event, arg):
            if event == 'return':
                self._locals = frame.f_locals.copy()

            # tracer is activated on next call, return or exception

        sys.setprofile(tracer)
        def decorated( *args, **kwargs):




                result = fn(*args, **kwargs)
                keys = list(self._locals.keys())
                values = list(self._locals.values())

                for i in range (len(keys)) :

                    self.logger.debug("[@{0}] variable {1} =  {2} ".format(fn.__name__, keys[i], values[i]))
                return result


        return decorated

# persistent_locals2 has been co-authored with Andrea Maffezzoli
import sys
## Start example

@LogDecorator()
def example(a, b, c):
    a = b + c
    b = a + c
    c = a + b
    d = "hi there"
    result = a + b + c
    return result



example(5,4,3)






